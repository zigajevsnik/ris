<!DOCTYPE html>
<html>
<?php
session_start();
?>
<head>
<meta charset="UTF-8">
<title>prijava</title>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css"
href=https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
</head>

<body>
<div class= "container">
    <div class="login-box">
    <div class= "row">
    <div class= "col-md-6 "login-left>
        <h2> Prijava </h2>
        <form action = "validation.php" method= "post">
            <div class ="form-group">
                <label>Uporabnisko ime</label>
                <input type="text" name="user" class="form-control" required>
                </div>
            <div class="form-group">
            <label> Geslo </label>
            <input type="password" name="password" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary"> Vpis </button>
        </form>
     </div>

     <div class= "col-md-6 "login-right>
        <h2> Registracija </h2>
        <form action = "registracija.php" method= "post">
            <div class ="form-group">
                <label>Uporabnisko ime</label>
                <input type="text" name="user" class="form-control" required>
                </div>
            <div class="form-group">
            <label> Geslo </label>
            <input type="password" name="password" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary"> Registriraj </button>
        </form>
     </div>
    </div>
</div>
</div>

</body>

</html>